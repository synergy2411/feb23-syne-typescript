export default class ListTemplate {
    constructor(ulContainer) {
        this.ulContainer = ulContainer;
    }
    render(selType, doc) {
        const liElement = document.createElement("li");
        liElement.classList.add('list-group-item', 'mb-2', 'd-flex');
        liElement.innerHTML = `
            <h2>${selType.toUpperCase()}</h2>
            <p>${doc.format()}</p>
        `;
        this.ulContainer.appendChild(liElement);
    }
}
//# sourceMappingURL=list-template.js.map