export default class Payment {
    constructor(expense) {
        this.expense = expense;
    }
    format() {
        return `Made a payment of $${this.expense.amount} on account for ${this.expense.title}!`;
    }
}
//# sourceMappingURL=payment.js.map