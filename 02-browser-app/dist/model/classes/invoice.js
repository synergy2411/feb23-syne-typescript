export default class Invoice {
    constructor(invoice) {
        this.invoice = invoice;
    }
    format() {
        return `Received payment of $${this.invoice.amount} on account for ${this.invoice.title}!`;
    }
}
//# sourceMappingURL=invoice.js.map