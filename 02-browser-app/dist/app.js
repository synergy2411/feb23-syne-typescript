import Payment from './model/classes/payment.js';
import Invoice from './model/classes/invoice.js';
import ListTemplate from './controller/list-template.js';
window.onload = function () {
    const buttonCancelEl = document.querySelector("#buttonCancel");
    const buttonAddEl = document.querySelector("#buttonAdd");
    const inputTitleEl = document.querySelector("#inputTitle");
    const sltTypeEl = document.querySelector("#sltType");
    const inputAmountEl = document.querySelector("#inputAmount");
    const inputDateEl = document.querySelector("#inputDate");
    const ulContainerEl = document.querySelector("#listContainer");
    let listTemplate = new ListTemplate(ulContainerEl);
    buttonAddEl.addEventListener("click", function (ev) {
        ev.preventDefault();
        let exp = {
            title: inputTitleEl.value,
            amount: Number(inputAmountEl.value),
            type: sltTypeEl.value,
            date: new Date(inputDateEl.value)
        };
        let doc;
        if (sltTypeEl.value === 'payment') {
            doc = new Payment(exp);
        }
        else {
            doc = new Invoice(exp);
        }
        listTemplate.render(sltTypeEl.value, doc);
    });
    buttonCancelEl.addEventListener("click", function (ev) {
        ev.preventDefault();
        inputTitleEl.value = '';
        inputAmountEl.value = '0.0';
        sltTypeEl.value = '';
        inputDateEl.value = '';
    });
};
//# sourceMappingURL=app.js.map