import { IFormatter } from "../model/formatter.interface";

export default class ListTemplate {
    constructor(private ulContainer: HTMLUListElement) { }

    render(selType: string, doc: IFormatter) {
        const liElement = document.createElement("li");
        liElement.classList.add('list-group-item', 'mb-2', 'd-flex');
        liElement.innerHTML = `
            <h2>${selType.toUpperCase()}</h2>
            <p>${doc.format()}</p>
        `
        this.ulContainer.appendChild(liElement);
    }
}