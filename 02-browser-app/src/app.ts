import { IExpense } from './model/expense.interface.js';
import { IFormatter } from './model/formatter.interface.js';
import Payment from './model/classes/payment.js';
import Invoice from './model/classes/invoice.js';
import ListTemplate from './controller/list-template.js';


window.onload = function () {

    const buttonCancelEl = document.querySelector("#buttonCancel") as HTMLButtonElement
    const buttonAddEl = document.querySelector("#buttonAdd") as HTMLButtonElement
    const inputTitleEl = document.querySelector("#inputTitle") as HTMLInputElement
    const sltTypeEl = document.querySelector("#sltType") as HTMLSelectElement
    const inputAmountEl = document.querySelector("#inputAmount") as HTMLInputElement
    const inputDateEl = document.querySelector("#inputDate") as HTMLInputElement
    const ulContainerEl = document.querySelector("#listContainer") as HTMLUListElement

    let listTemplate = new ListTemplate(ulContainerEl)

    buttonAddEl.addEventListener("click", function (ev: MouseEvent) {
        ev.preventDefault();
        let exp: IExpense = {
            title: inputTitleEl.value,
            amount: Number(inputAmountEl.value),
            type: sltTypeEl.value,
            date: new Date(inputDateEl.value)
        }

        let doc: IFormatter;

        if (sltTypeEl.value === 'payment') {
            doc = new Payment(exp)
        } else {
            doc = new Invoice(exp)
        }

        listTemplate.render(sltTypeEl.value, doc)
    })


    buttonCancelEl.addEventListener("click", function (ev: MouseEvent) {
        ev.preventDefault();
        inputTitleEl.value = '';
        inputAmountEl.value = '0.0';
        sltTypeEl.value = '';
        inputDateEl.value = ''
    })
}