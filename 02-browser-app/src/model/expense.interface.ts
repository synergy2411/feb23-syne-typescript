export interface IExpense {
    type: string;
    title: string;
    amount: number;
    date: Date;
}