import { IFormatter } from "../formatter.interface"
import { IExpense } from "../expense.interface"

export default class Payment implements IFormatter {
    constructor(private expense: IExpense) { }
    format(): string {
        return `Made a payment of $${this.expense.amount} on account for ${this.expense.title}!`
    }
}