import { IFormatter } from "../formatter.interface";
import { IExpense } from "../expense.interface";

export default class Invoice implements IFormatter {
    constructor(private invoice: IExpense) { }
    format(): string {
        return `Received payment of $${this.invoice.amount} on account for ${this.invoice.title}!`
    }
}