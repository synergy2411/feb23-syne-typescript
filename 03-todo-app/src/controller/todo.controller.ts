interface ITodo {
    id: string;
    label: string;
}

export default class Todo {
    private todoCollection: Array<ITodo> = [];

    addTodo(label: string) {
        let newItem = {
            id: "T00" + (this.todoCollection.length + 1),
            label
        }
        this.todoCollection.push(newItem)
    }

    removeTodo(todoId: string) {
        const position = this.todoCollection.findIndex(todo => todo.id === todoId)
        const deletedItem = this.todoCollection.splice(position, 1);
        return deletedItem[0]
    }

    getTodos() {
        return this.todoCollection.slice(0)
    }
}