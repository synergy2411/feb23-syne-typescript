import Todo from "./controller/todo.controller.js";
import TodoListTemplate from "./controller/list-todo.js";

window.onload = function () {
    const buttonAddEl = document.querySelector("#buttonAdd") as HTMLButtonElement
    const inputLabelEl = document.querySelector("#inputLabel") as HTMLInputElement
    const todoContainer = document.querySelector("#todoContainer") as HTMLUListElement
    let todo = new Todo();
    let listTemplate = new TodoListTemplate(todoContainer, todo)

    buttonAddEl.addEventListener("click", function (ev: MouseEvent) {
        ev.preventDefault();
        todo.addTodo(inputLabelEl.value);
        listTemplate.render()
        inputLabelEl.value = ''
    })
}