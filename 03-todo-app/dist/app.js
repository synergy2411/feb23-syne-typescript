import Todo from "./controller/todo.controller.js";
import TodoListTemplate from "./controller/list-todo.js";
window.onload = function () {
    const buttonAddEl = document.querySelector("#buttonAdd");
    const inputLabelEl = document.querySelector("#inputLabel");
    const todoContainer = document.querySelector("#todoContainer");
    let todo = new Todo();
    let listTemplate = new TodoListTemplate(todoContainer, todo);
    buttonAddEl.addEventListener("click", function (ev) {
        ev.preventDefault();
        todo.addTodo(inputLabelEl.value);
        listTemplate.render();
        inputLabelEl.value = '';
    });
};
