export default class TodoListTemplate {
    constructor(todoContainer, todo) {
        this.todoContainer = todoContainer;
        this.todo = todo;
    }
    onDeleteItem(todoId) {
        this.todo.removeTodo(todoId);
        this.render();
    }
    render() {
        this.todoContainer.innerHTML = '';
        this.todo.getTodos().forEach(todo => {
            const liElement = document.createElement("liElement");
            liElement.classList.add('list-group-item', 'm-4', 'd-flex', 'justify-content-between');
            liElement.innerHTML = todo.label.toUpperCase();
            liElement.setAttribute("id", todo.id);
            const deleteButtonEl = document.createElement("button");
            deleteButtonEl.classList.add('btn', 'btn-outline-danger', 'delete-btn', 'btn-sm');
            deleteButtonEl.textContent = "❌";
            deleteButtonEl.addEventListener("click", () => this.onDeleteItem(todo.id));
            liElement.appendChild(deleteButtonEl);
            this.todoContainer.appendChild(liElement);
        });
    }
}
