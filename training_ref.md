# Break Timing

- Tea Break > 11:00AM (15 Mins)
- Lunch > 1:00PM (45 Mins)
- Tea Break > 3:30PM (15 Mins)

# JavaScript

- Don't need to compile the code
- JS runs on both client side (Browser Env) and server side(Node Runtime Env)
- Single Threaded
- Flexible
- Event-driven
- Fast
- Non-blocking
- Asynchronous (Callback, Promises, Async...await, Observables)
- light-weight
- Dynamically type - type determined at run time

# Types in JavaScript

- Primitive Types : String, Number, Boolean
- Reference Types : Object, Array, Function, Date

# TypeScript

- Early compile time errors
- Sponsured & Maintained by Microsoft Team
- Need to transpile the TS code in JavaScript
- Superset of JavaScript
- Type System - Type Robust App
- ES6+ features (Arrow functions, destructuring, rest/spread, template literals etc)
- Object Oriented features - Class, Inheritance, Interface, Abstract Classes etc
- Additional Types : any, never, void, enum, unknown, tuple, union etc

# npx tsc --init -> generate tsconfig.json

# Module System

- CommonJS : default Module system for Node (module, exports, require())
- ESM : default module system for browser (import, export)
- UMD : Reusable Package
- AMD : Async Module Definition

- npm init -y (create package.json file)
- npm install typescript
- npm install nodemon

- npm run build
- npm run dev:start

https://gitlab.com/synergy2411/feb23-syne-typescript

npm init -y
npx tsc --init
npm install typescript
add Live-Server extension

tsconfid.json
module : "es2015",
rootSrc : "./src",
outDir :"./dist"
sourceMap : true,
declaration : true,
