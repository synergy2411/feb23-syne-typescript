"use strict";
// DECORATORS : simple function prefixed with '@', used for meta-programming
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Classes, Methods, Property, Parameters, Accessor/Mutator
// Decorator
// function seal(fn: Function){
//     Object.seal(fn);
//     Object.seal(fn.prototype)
// }
// Decorator Factory
// function seal(){
//     return function(fn : Function){
//         Object.seal(fn);
//         Object.seal(fn.prototype)
//     }
// }
// @seal()
// class Test{}
// let friendList = ["Joe", "Monica", "Ross"];
// function allowFriend(target: any, member: string) {
//     let currentFriend = target[member];
//     Object.defineProperty(target, member, {
//         set: (value) => {
//             if (friendList.includes(value)) {
//                 currentFriend = value;
//             }
//             return;
//         },
//         get: () => currentFriend
//     })
// }
function allowFriend(friendList) {
    return function (target, member) {
        let currentFriend = target[member];
        Object.defineProperty(target, member, {
            set: (value) => {
                if (friendList.includes(value)) {
                    currentFriend = value;
                }
                return;
            },
            get: () => currentFriend
        });
    };
}
class MyFriends {
    constructor() {
        this.theFriend = 'Joe';
    }
}
__decorate([
    allowFriend(["Joe", "Monica", "Ross"]),
    __metadata("design:type", String)
], MyFriends.prototype, "theFriend", void 0);
let friend = new MyFriends();
console.log(friend.theFriend);
friend.theFriend = "Nobody";
console.log(friend.theFriend);
friend.theFriend = "Ross";
console.log(friend.theFriend);
