"use strict";
// GENERICS
// let numbers : Array<string> = []
// interface Resource<T,U>{
//     resourceId : T,
//     resourceName : U
// }
// let serverOne : Resource<number, string> = {
//     resourceId : 201,
//     resourceName : "Server 01"
// }
// let serverTwo : Resource<string, string> = {
//     resourceId : "R001",
//     resourceName : "Server 02"
// }
// interface Stackable <T>{
//     push(item : T) : void;
//     pop() : void;
// }
// class Stack<T> implements Stackable<T>{
//     private elements: Array<T> = [];
//     constructor(private size: number) { }
//     push(item: T) {
//         if (this.elements.length < this.size) {
//             this.elements.push(item);
//             return;
//         }
//         throw new Error("Stack is filled already!!")
//     }
//     pop() {
//         this.elements.pop()
//     }
//     getLength() {
//         return this.elements.length;
//     }
// }
// let stringStack = new Stack<string>(5);
// stringStack.push("TypeScript")
// stringStack.push("Angular")
// stringStack.push("React")
// stringStack.push("Vue")
// stringStack.push("Node")
// console.log("Stack Length : ", stringStack.getLength());
// stringStack.pop()
// console.log("Stack Length [AFTER POP] : ", stringStack.getLength());
// function getLength<T extends { length: number }>(args: T): number {
//     return args.length;
// }
// let arrLength = getLength<string[]>(["Foo", "Bar", "Bam"])
// console.log(arrLength);
// let strOne = "Welcome to TypeScript";
// let strLength = getLength<string>(strOne);
// console.log(strLength);
// let numberOne = 101;
// let numberLength = getLength(numberOne);
// function merge<K extends object, U extends object>(x: K, y: U) {
//     return {
//         ...x,
//         ...y
//     }
// }
// let mergedTwo = merge({ name: "jenny" }, { age: 34 })
// console.log(mergedTwo);
// let mergedUser = merge({ name: "john doe" }, { email: "john@test.com" });
// console.log(mergedUser)
// function addAtBeginning<T>(val: T, arr: T[]): T[] {
//     return [val, ...arr];
// }
// interface IUser {
//     userId: string;
//     username: string;
// }
// let users: IUser[] = [{ userId: "u002", username: "jenny" }, { userId: "u003", username: "james" }];
// let usersArr = addAtBeginning<IUser>({ userId: "u001", username: "John" }, users);
// let numArr = addAtBeginning<number>(101, [100, 99, 98, 97])
// let strArr = addAtBeginning<string>("Hello", ["World", "!!!"])
// console.log(strArr[0].length)
// FUNCTION OVERLOADING
// function addAtBeginning(val: string, arr: string[]):string[];
// function addAtBeginning(val: number , arr: number[]): number[];
// function addAtBeginning(val: object, arr:object[]): object[];
// function addAtBeginning(val: any, arr: any[]) : any[]{
//     return [val, ...arr]
// }
// let numbersArray = addAtBeginning(101, [201, 301, 401]);
// let stringArr = addAtBeginning("Hello", ["World", "!!"])
