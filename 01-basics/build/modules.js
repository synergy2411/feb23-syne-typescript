"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Named Import (heavily used in Angular)
const fortune_1 = require("./utils/fortune");
// Default Import (heavily used in React)
const fortune_2 = __importDefault(require("./utils/fortune"));
let john = new fortune_2.default("S001", "John Doe");
console.log("Student Details : ", john.getDetails());
console.log("Lucky Number of the day : ", (0, fortune_1.getLuckyNumber)());
