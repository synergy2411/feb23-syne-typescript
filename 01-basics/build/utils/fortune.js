"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLuckyNumber = void 0;
const RANDOM_NUMBER = Math.round(Math.random() * 100);
const getLuckyNumber = () => RANDOM_NUMBER;
exports.getLuckyNumber = getLuckyNumber;
class Student {
    constructor(studId, studName) {
        this.studId = studId;
        this.studName = studName;
    }
    getDetails() {
        return this.studId + " - " + this.studName;
    }
}
exports.default = Student;
