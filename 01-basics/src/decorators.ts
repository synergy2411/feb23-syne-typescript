// DECORATORS : simple function prefixed with '@', used for meta-programming

// Classes, Methods, Property, Parameters, Accessor/Mutator

// Decorator
// function seal(fn: Function){
//     Object.seal(fn);
//     Object.seal(fn.prototype)
// }

// Decorator Factory
// function seal(){
//     return function(fn : Function){
//         Object.seal(fn);
//         Object.seal(fn.prototype)
//     }
// }

// @seal()
// class Test{}

// let friendList = ["Joe", "Monica", "Ross"];

// function allowFriend(target: any, member: string) {
//     let currentFriend = target[member];
//     Object.defineProperty(target, member, {
//         set: (value) => {
//             if (friendList.includes(value)) {
//                 currentFriend = value;
//             }
//             return;
//         },
//         get: () => currentFriend
//     })
// }


function allowFriend(friendList: Array<string>) {
    return function (target: any, member: string) {
        let currentFriend = target[member];
        Object.defineProperty(target, member, {
            set: (value) => {
                if (friendList.includes(value)) {
                    currentFriend = value;
                }
                return;
            },
            get: () => currentFriend
        })
    }
}


class MyFriends {
    @allowFriend(["Joe", "Monica", "Ross"])
    theFriend: string = 'Joe';
}

let friend = new MyFriends();
console.log(friend.theFriend)

friend.theFriend = "Nobody";
console.log(friend.theFriend)

friend.theFriend = "Ross"
console.log(friend.theFriend)