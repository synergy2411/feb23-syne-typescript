const RANDOM_NUMBER = Math.round(Math.random() * 100);

const getLuckyNumber = () => RANDOM_NUMBER;

export default class Student {
    constructor(private studId: string, private studName: string) { }
    getDetails() {
        return this.studId + " - " + this.studName
    }
}

export {
    getLuckyNumber
}