// Type Casting

// let x: any = "Hello World";

// let y = x as string;

// let z = <string>x;

// let abc = <number>x;

// console.log("ABC : ", abc.toFixed())












// Classes

// Overloading : creates the relationship between supplied arguments and return type

// function add(n1: number, n2: number): number;
// function add(n1: string, n2: string, n3?: string): string;
// function add(n1: any, n2: any, n3?: any): any {

//     if (n3) {
//         return n1 + n2 + n3
//     }

//     return n1 + n2
// }


// let result = add(2, 4);

// let resultTwo = add("Hello ", "World", "!!!")
// console.log(resultTwo);














// function add(x: number | string, y: number | string): number | string {
//     if (typeof x === 'number' && typeof y === 'number') {
//         return x + y
//     }
//     if (typeof x === 'string' && typeof y === 'string') {
//         return x + y
//     }
//     return '';
// }

// console.log(add(2, 3))

// console.log(add("John ", "Doe"));

// const result = add(3,5);

// const resultTwo = add("", "")





// function addStr(s1: string, s2: string): string {
//     return s1 + s2;
// }


// function addNum(n1: number, n2: number): number {
//     return n1 + n2;
// }


// console.log(addNum(4, 5))
// console.log(addStr("Hello", " World"))










// Interfaces : Full Abstraction; implementation required

// interface Formatter {
//     formatString: (str: string, isUpperCase: boolean) => string;
// }

// class Format implements Formatter {
//     formatString(str: string, isUpperCase: boolean): string {
//         if (isUpperCase) {
//             return str.toUpperCase()
//         } else {
//             return str.toLowerCase()
//         }
//     };
// }

// let f = new Format();
// console.log(f.formatString("Hello world", true))
// console.log(f.formatString("Hello world", false))






// interface IProduct {
//     id?: string;
//     name: string;
//     brand: string;
//     price: number;
//     description?: string;
// }

// class Product {
//     private productList: Array<IProduct> = [];

//     constructor() { }

//     addProduct(product: IProduct) {
//         let newProduct = {
//             id: "P00" + (this.productList.length + 1),
//             ...product,
//         }
//         this.productList.push(newProduct)
//     }

//     getProducts(): Array<IProduct> {
//         return this.productList;
//     }

//     removeProduct(productId: string): IProduct | { message: string } {
//         const position = this.productList.findIndex(product => product.id === productId)
//         if (position >= 0) {
//             const deletedProducts = this.productList.splice(position, 1)
//             return deletedProducts[0]
//         } else {
//             return { message: "Unable to locate the product - " + productId }
//         }
//     }
// }

// let iPhone: IProduct = {
//     name: "iPhone 14",
//     brand: "Apple",
//     price: 199
// }

// let galaxy: IProduct = {
//     name: "Galaxy",
//     brand: "Samsung",
//     price: 99,
//     description: "Awesome Phone"
// }

// let newProduct = new Product();

// newProduct.addProduct(iPhone);
// newProduct.addProduct(galaxy);

// console.log("[BEFORE DELETE]", newProduct.getProducts())


// let deletedItem = newProduct.removeProduct("P001")

// console.log("[DELETED ITEM]", deletedItem);

// console.log("[AFTER DELETE]", newProduct.getProducts());














// Abstract Classes - define hhe behaviour of child class; Can't create the instance of Abstract classess;

// abstract class Student {
//     constructor(private firstName: string, private lastName: string) { }

//     getFullName() {
//         return this.firstName + " " + this.lastName
//     }

//     abstract getDuration(): number;

//     getFee() {
//         return `${this.firstName} ${this.lastName} should pay the fees of ${this.getDuration() * 1000}`
//     }
// }

// class RegularStudent extends Student {
//     constructor(firstName: string, lastName: string, private duration: number) {
//         super(firstName, lastName)
//     }
//     getDuration(): number {
//         return this.duration;
//     }
// }

// class PartTimeStudent extends Student {
//     constructor(firstName: string, lastName: string, private duration: number) {
//         super(firstName, lastName)
//     }
//     getDuration(): number {
//         return this.duration;
//     }
// }

// let john = new RegularStudent("John", "Doe", 8)

// let jenny = new PartTimeStudent("Jenny", "Doe", 4)

// console.log(john.getFee())

// console.log(jenny.getFee())












// Static keyword : shared among all the instances of the class; Can only be accessed using Classname

// class BuildTicket {
//     private static numOfPassengers = 0;

//     constructor() {
//         BuildTicket.numOfPassengers++
//     }

//     static getNumberOfPassengers() {
//         return BuildTicket.numOfPassengers;
//     }
// }

// let foo = new BuildTicket()
// let bar = new BuildTicket()

// console.log(BuildTicket.getNumberOfPassengers())






// Inheritance

// class Person {
//     constructor(private firstName: string, private lastName: string) { }

//     private getFullName() {
//         return this.firstName + " " + this.lastName
//     }

//     protected aboutMe() {
//         return `This is ${this.getFullName()}!!`
//     }
// }

// class Employee extends Person {
//     constructor(firstName: string, lastName: string, private jobTitle: string) {
//         super(firstName, lastName)
//     }
//     aboutMe(): string {
//         return `${super.aboutMe()} I'm also a ${this.jobTitle}`
//     }
// }

// let alice = new Employee("Alice", "Doe", "Developer");

// console.log(alice.aboutMe())




// class Student {
//     private _studAge: number;
//     constructor(private studId: string, studAge: number) {
//         this._studAge = studAge
//     }

//     // Getter | Accessor
//     get age() {
//         return this._studAge
//     }

//     // Setter | Mutator
//     set age(value: number) {
//         if (value > 20 && value < 60) {
//             this._studAge = value
//         }
//     }

// }


// let john = new Student("S0101", 23);
// console.log("Age : ", john.age);
// john.age = 24;
// john.age = 19;
// john.age = 62;
// console.log("Age : ", john.age);





















// class Employee {
    // private empId: string;
    // empName: string;

    // constructor(empId: string, empName: string) {
    //     this.empId = empId;
    //     this.empName = empName;
    // }

    // Constructor injection
//     constructor(readonly empId: string, private empName: string) { }

//     getDetails(): string {
//         return `${this.empId} - ${this.empName}`
//     }
// }

// let jenny = new Employee("E001", "Jenny Doe");

// console.log(jenny.getDetails())
// console.log("Employee ID : ", jenny.empId);

// jenny.empId = "E1010"





















// PROMISES

// Producer Code

// const demoPromise = (ms: number) => {
//     const promise = new Promise((resolved, rejected) => {
//         if (ms < 2000) {
//             setTimeout(() => {
//                 resolved({ message: "SUCCESS" })
//             }, ms);
//         } else {
//             rejected(new Error("Too long wait..."))
//         }
//     })
//     return promise;
// }

// Consumer Code
//  - .then().catch()

// const demoPromiseConsumer = () => {
//     demoPromise(3000)
//         .then((response) => {
//             console.log("[RESPONSE]", response)
//         }).catch(err => console.error(err))
// }

// demoPromiseConsumer();

//  - Async...await

// const demoPromiseConsumerTwo = async () => {
//     try {
//         const response = await demoPromise(1800);
//         console.log("[RESPONSE TWO]", response)
//     } catch (err) {
//         console.error(err);
//     }
// }

// demoPromiseConsumerTwo();



// let vs const

// const userA = {
//     name: "Foo"
// }


// userA.name = "Bar";

// console.log(userA);         // ?


// userA = {
//     name : "Jenny"
// }


// const fruits = ["apple", "kiwi", "guava"]

// fruits.push("oranges");

// console.log(fruits);

// fruits = []






// rest vs spread

// const demo = (...args: string[]) => {
//     console.log(args);
// }

// demo("Hello", "World", "!!!")
// demo("Hello", "World")
// demo("Hello")


// let numbers = [3, 4, 5];

// let moreNumbers = [1, 2, ...numbers, 6, 7, 8];

// console.log(moreNumbers);


// let userA = {
//     name: "John",
//     age: 32
// }

// let userB = {
//     ...userA,
//     email: "b@test.com",
//     age: 34
// }

// console.log(userB);         // ?