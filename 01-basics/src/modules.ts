// Named Import (heavily used in Angular)
import { getLuckyNumber } from './utils/fortune';

// Default Import (heavily used in React)
import TheStudent from './utils/fortune';

let john = new TheStudent("S001", "John Doe")

console.log("Student Details : ", john.getDetails());

console.log("Lucky Number of the day : ", getLuckyNumber());