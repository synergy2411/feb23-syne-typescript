// Types in TypeScript



// Type Gaurds - narrowing down the types
// - typeof
// - instanceof


// Intersection Type (&) - combination of various other types

type EmployeeType = {
    name: string;
    empId: string;
}

type StudentType = {
    name: string;
    courseEnrolled: string;
}

type EmployeeStudentType = EmployeeType & StudentType;

let userOne: EmployeeStudentType;

userOne = {
    name: "John Doe",
    empId: "E001",
    courseEnrolled: "TypeScript"
}



// Union Type (|) : allows to store a value of one or several types

// type MyUserAgeType = string | number | boolean;

// let userAge : MyUserAgeType;

// userAge = "Thirty-Two";

// userAge = 32;













// FUNCTION


// let mul: (n1: number, n2: number) => number;

// mul = function (n1: number, n2: number) {
//     return n1 * n2;
// }

// console.log(mul(3, 5));

// let add: Function;

// add = function (n1: number, n2: number) {
//     return n1 + n2;
// }

// console.log(add(3, 5));







// ENUM - group of constant values; by default, starting with 0

// enum Day {
//     Monday = 101,
//     Tuesday,
//     Wednesday,
//     Thursday,
//     Friday,
//     Saturday,
//     Sunday
// }

// let weekOff: Day;

// weekOff = Day.Saturday;

// console.log(weekOff);













// TUPLE - number of elements are fixed, element type is also fixed

// let hobby : [string, number];

// hobby = ["Yoga", 4]

// hobby = [5, "swimming"]




// ARRAY

// let strArray : string[];

// strArray = ["Hello", "World"];

// strArray.push("!!")

// console.log(strArray);



// let friends: Array<string>;

// friends = ["Monica", "Ross", "Rachel", "Joe"];

// let bestFriend = friends[1];

// console.log(bestFriend.toLocaleUpperCase())



// let numbers: Array<number>;

// numbers = [201, 202, 203, 204];

// let luckyNumber = numbers[2];

// console.log(luckyNumber.toFixed())
















// OBJECT - can hold the reference types

// type MyProductType = {
//     name : string;
//     brand? : string;                // optional property (?)
//     price : number;
// }


// let productTwo : MyProductType;

// productTwo = {
//     name : "Galaxy",
//     // brand : "Samsung",
//     price : 99
// }

// console.log(productTwo.name)


// let product : object;

// product = {
//     name : "iPhone 14",
//     brand : "Apple",
//     price : 199
// }

// product = [];

// product = () => {}

// console.log(product);


// product = "";

// product = 32;

// product = true













// NULL : type as well as value

// let isUserAvailable : null;

// isUserAvailable = null;

// isUserAvailable = true;

// isUserAvailable = "";

// isUserAvailable = 32;

// isUserAvailable = undefined;

// isUserAvailable = { }









// UNKNOWN : can be assigned to other types only after checking UNKNOW type

// let anyVar : any;

// anyVar = "John Doe"

// let xyz : unknown;

// xyz = "Jenny Doe";

// let username : string;

// username = anyVar;

// if(typeof xyz === 'string'){
//     username = xyz;
// }















// NEVER - use with the function which always throw the error

// function neverStop() : never {
//     while(true){
//         console.log("Never returns");
//     }
// }

// function throwError(): never{
//     throw new Error("Something wrong")
// }


// let neverVar : never;

// neverVar = "";

// neverVar = 32;

// neverVar = null;

// neverVar = undefined;








// VOID - does not return anything; opposite of ANY type; Used as function return type;

// let useless : void;

// useless = ""

// useless = 32

// useless = null;

// useless = undefined;








// ANY - skip the type checking; No intellisence
// let anyVar : any;

// anyVar = "Hello World";

// anyVar = 32;

// anyVar = true;






















// Structural Types - 'type' keyword | class | interface

// type MyStringType = string;

// let username: MyStringType = "John Doe";

// console.log(username.length)






// Explicit Type | Type annotation
// Where you don't want to specify the value at the time of declaring the variable

// let userName: string;

// userName = "John Doe";




// Implicit Typing | Type inference
// Always use Type Inference

// let userAge = 32;

// userAge = "Thirty Two";

// let userName = "John Doe";

// userName = true;